<?php
namespace Linets\Checkout\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Customer\Setup\CustomerSetupFactory; 

class ConfigureAddressAttributes implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    protected $writerInterface;

    /**
     * @var \Magento\Customer\Setup\CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $configWriter;


    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writerInterface
     * @param CustomerSetupFactory $customerSetupFactory
     * @param WriterInterface $configWriter
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writerInterface,
        CustomerSetupFactory $customerSetupFactory,
        WriterInterface $configWriter
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writerInterface = $writerInterface;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->configWriter = $configWriter;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

        // Multiline para direccion en 4
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'street');
        $attribute->setData('multiline_count', 3);
        $attribute->save();

        //update attributes company, postcode, country_id
        $customerSetup->updateAttribute('customer_address', 'company', 'is_visible', false);
        $customerSetup->updateAttribute('customer_address', 'postcode', 'is_visible', false);
        $customerSetup->updateAttribute('customer_address', 'postcode', 'is_required', false);
        $customerSetup->updateAttribute('customer_address', 'postcode', 'default_value', '*');
        $customerSetup->updateAttribute('customer_address', 'country_id', 'default_value', 'CL');

        $this->configWriter->save(
            'general/country/optional_zip_countries',
            'CL',
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            0
        );

        $this->moduleDataSetup->endSetup();
    }


    public function setConfigData($path, $value){
        $this->writerInterface->save($path, $value);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
