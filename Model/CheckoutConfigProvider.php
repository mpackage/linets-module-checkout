<?php
namespace Linets\Checkout\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Store\Model\ScopeInterface;

class CheckoutConfigProvider implements ConfigProviderInterface
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;


    /**
     * constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(){
        $config = [];

        $googleApiKey = $this->_scopeConfig->getValue('cms/pagebuilder/google_maps_api_key', ScopeInterface::SCOPE_STORE);
        $filterStoresByRegion = $this->_scopeConfig->isSetFlag('custom_checkout/storepickup/filter_region', ScopeInterface::SCOPE_STORE);
        $progressiveSteps = $this->_scopeConfig->isSetFlag('custom_checkout/checkout/use_steps_confirmation', ScopeInterface::SCOPE_STORE);

        $config['linets'] = [
            'delivery_html' => $this->_scopeConfig->getValue('custom_checkout/checkout/delivery_html', ScopeInterface::SCOPE_STORE),
            'store_pickup_html' => $this->_scopeConfig->getValue('custom_checkout/checkout/store_pickup_html', ScopeInterface::SCOPE_STORE),
            'use_steps_confirmation' => $progressiveSteps,
            'allow_continue_email_exist' => ($progressiveSteps) ? $this->_scopeConfig->isSetFlag('linets/checkout/allow_continue_email_exist', ScopeInterface::SCOPE_STORE):  1,
            'google_maps_api_key' => $googleApiKey,
            'passwordview' => $this->_scopeConfig->isSetFlag('custom_checkout/general/viewpassword', ScopeInterface::SCOPE_STORE),
            'show_location_map' => ($googleApiKey)? $this->_scopeConfig->isSetFlag('custom_checkout/checkout/show_location_map', ScopeInterface::SCOPE_STORE) : 0,
            'pickup_filter_region' => $filterStoresByRegion,
            'pickup_filter_city' => $filterStoresByRegion && $this->_scopeConfig->isSetFlag('linets/storepickup/filter_city', ScopeInterface::SCOPE_STORE),
            'pickup_closest_store' => $this->_scopeConfig->isSetFlag('custom_checkout/storepickup/closest_store', ScopeInterface::SCOPE_STORE),
            'pickup_static_maps' => $this->_scopeConfig->isSetFlag('custom_checkout/storepickup/image_maps', ScopeInterface::SCOPE_STORE),
        ];

        return $config;
    }
}
