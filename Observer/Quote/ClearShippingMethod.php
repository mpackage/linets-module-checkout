<?php
namespace Linets\Checkout\Observer\Quote;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Quote\Api\CartRepositoryInterface;

class ClearShippingMethod implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;


    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;


    /**
     * Sidebar constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param CartRepositoryInterface $quoteRepository
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CheckoutSession $checkoutSession,
        CartRepositoryInterface $quoteRepository
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer){
        if (!$this->scopeConfig->isSetFlag('linets/cart/cart_estimate', ScopeInterface::SCOPE_STORE)){
            $quote = $this->checkoutSession->getQuote();
            if ($quote){
                $shippingAddress = $quote->getShippingAddress();
                if ($shippingAddress && $shippingAddress->getShippingMethod()){
                    $shippingAddress->setShippingMethod(false);
                    $shippingAddress->removeAllShippingRates();
                    $shippingAddress->setCollectShippingRates(false);
                    if ($quote->getExtensionAttributes() && $quote->getExtensionAttributes()->getShippingAssignments()){
                        $shippingAssignments = $quote->getExtensionAttributes()->getShippingAssignments();
                        foreach ($shippingAssignments as $shippingAssignment){
                            $shippingAssignment->getShipping()->setMethod(null);
                        }
                        $extensionAttributes = $quote->getExtensionAttributes();
                        $extensionAttributes->setShippingAssignments($shippingAssignments);
                        $quote->setExtensionAttributes($extensionAttributes);
                    }
                    $quote->collectTotals();
                    $this->quoteRepository->save($quote);
                }
            }
        }
    }
}
