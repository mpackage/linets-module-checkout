<?php
declare(strict_types=1);

namespace Linets\Checkout\Block\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Checkout\Helper\Data as CheckoutHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class LayoutProcessor
 */
class LayoutProcessor implements LayoutProcessorInterface
{
    /**
     * @var CheckoutHelper
     */
    protected $checkoutHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * LayoutProcessor constructor.
     * @param CheckoutHelper $checkoutHelper
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        CheckoutHelper $checkoutHelper,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->checkoutHelper = $checkoutHelper;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Process js Layout of block
     *
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        $jsLayout = $this->shippingFields($jsLayout);
        $jsLayout = $this->billingFields($jsLayout);
        $jsLayout = $this->changeBillingAddressPosition($jsLayout);
        return $jsLayout;
    }


    protected function shippingFields($jsLayout){
        $fields = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];
        $fields = $this->formFieldChanges($fields, 0, $jsLayout);
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'] = $fields;

        return $jsLayout;
    }

    protected function billingFields($jsLayout){
        $isDisplayBillingOnPaymentMethodAvailable = $this->checkoutHelper->isDisplayBillingOnPaymentMethodAvailable();

        if (!$isDisplayBillingOnPaymentMethodAvailable) {
            $fields = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields'];
            $fields['deps'] = [
                'checkoutProvider'
            ];
            $fields['children'] = $this->formFieldChanges($fields['children'], 1, $jsLayout);

            // REPLACE CHILDREN
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields'] = $fields;
        } else {
            $paymentMethodRenders = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']
            ['children']['payment']['children']['payments-list']['children'];
            if (is_array($paymentMethodRenders)) {
                foreach ($paymentMethodRenders as $name => $renderer) {
                    if (isset($renderer['children']) && array_key_exists('form-fields', $renderer['children'])) {
                        $fields = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$name]['children']['form-fields']['children'];
                        $fields = $this->formFieldChanges($fields, 1, $jsLayout);
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$name]['children']['form-fields']['children'] = $fields;
                    }
                }
            }
        }

        return $jsLayout;
    }

    public function formFieldChanges($fields, $type = null, $jsLayout = []){
        // ADD ADDITIONAL CLASSES
        foreach($fields as $childrenKey => $children){
            if (isset($fields[$childrenKey]['additionalClasses'])){
                $fields[$childrenKey]['additionalClasses'] .= ' ' . $childrenKey;
            } else {
                $fields[$childrenKey]['additionalClasses'] = $childrenKey;
            }
        }

        $fields['telephone']['config']['tooltip'] = null;

        if (isset($jsLayout['components']['checkoutProvider']['dictionaries']['country_id'])){
            $fields['country_id']['visible'] = false;
        }
        
        if (isset($fields['postcode'])){
            $fields['postcode']['visible'] = false;
        }

        if (isset($fields['street'])){
            $fields['street']['config']['template'] = 'Linets_Checkout/group/group';
            $fields['street']['sortOrder'] = 400;

            $fields['street']['children'][0]['label'] = $fields['street']['label'];
            unset($fields['street']['label']);
            $fields['street']['children'][0]['additionalClasses'] = 'street';
            $fields['street']['children'][0]['label'] = __('Street Address: Line 1');

            $fields['street']['children'][1]['validation']['required-entry'] = 1;
            $fields['street']['children'][1]['label'] = __('Street Address: Line 2');
            $fields['street']['children'][1]['additionalClasses'] = 'number';
            $fields['street']['children'][1]['visible'] = true;

            if (isset($fields['street']['children'][2])){
                $fields['street']['children'][2]['label'] = __('Street Address: Line 3');
                $fields['street']['children'][2]['additionalClasses'] = 'floor';
            }
            if (isset($fields['street']['children'][3])){
                $fields['street']['children'][3]['label'] = __('Street Address: Line 4');
                $fields['street']['children'][3]['additionalClasses'] = 'observations';
            }
        }

        if (isset($fields['inline-form-manipulator'])){
            $fields['inline-form-manipulator']['sortOrder'] = 300;
        }
        return $fields;
    }

    protected function changeBillingAddressPosition($jsLayout){
        if ($this->checkoutHelper->isDisplayBillingOnPaymentMethodAvailable() === false) {
            $afterMethodChildren = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children'];
            if(isset($afterMethodChildren['billing-address-form'])){
                $billingAddressComponent = $afterMethodChildren['billing-address-form'];
                unset($afterMethodChildren['billing-address-form']);
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children'] = $afterMethodChildren;
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['billing-address-form'] = $billingAddressComponent;
            }
        }

        return $jsLayout;
    }
}
