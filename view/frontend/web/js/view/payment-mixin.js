/*jshint browser:true*/
/*global define*/
define([
    'ko',
    'Linets_Checkout/js/model/steps-visibility-manager'
], function (
    ko,
    stepsVisibilityManager
) {
    'use strict';

    var mixin = {
        useStepsConfirmation: stepsVisibilityManager.useStepsConfirmation,
        billingAddressStepConfirmed : stepsVisibilityManager.billingAddressStepConfirmed,
    };

    return function (target) {
        return target.extend(mixin);
    };
});
