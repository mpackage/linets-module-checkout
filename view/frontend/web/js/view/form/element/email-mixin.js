/*jshint browser:true*/
/*global define*/
define([
    'jquery',
    'ko',
    'Linets_Checkout/js/model/steps-visibility-manager',
    'Magento_Checkout/js/checkout-data'
], function ($, ko, stepsVisibilityManager, checkoutData) {
        'use strict';

        var mixin = {
            useStepsConfirmation: stepsVisibilityManager.useStepsConfirmation,
            stepConfirmed : stepsVisibilityManager.loginStepConfirmed,
            allowLoginContinueEmailExist : stepsVisibilityManager.allowLoginContinueEmailExist,
            isEmailValid : ko.observable(false),
            fromStepConfirmation : null,
            canConfirmStep : false,


            initObservable: function () {
                this._super();

                this.canConfirmStep = ko.computed(function(){
                    return !this.stepConfirmed() && (this.allowLoginContinueEmailExist || !this.isPasswordVisible())
                },this);

                return this;
            },

            emailHasChanged: function () {
                if (!this.useStepsConfirmation || this.isCustomerLoggedIn()){
                    return this._super();
                }

                if (this.fromStepConfirmation){
                    let checkDelay = this.checkDelay;
                    this.checkDelay = 0;
                    this._super();
                    this.checkDelay = checkDelay;
                } else {
                    if (this.stepConfirmed()){
                        this.unConfirmStep();
                    }
                    checkoutData.setCheckedEmailValue('');
                    this._super();
                    this.isPasswordVisible(false);
                    clearTimeout(this.emailCheckTimeout);
                }
            },

            /**
             * Local email validation.
             *
             * @param {Boolean} focused - input focus.
             * @returns {Boolean} - validation result.
             */
            validateEmail: function (focused) {
                var loginFormSelector = 'form[data-role=email-with-possible-login]',
                    loginForm = $(loginFormSelector);

                if (loginForm.length){
                    this.isEmailValid(this._super(focused));
                } else {
                    this.isEmailValid(false);
                }

                return this.isEmailValid();
            },

            validateEmailOnRender : function(){
                if (this.email()){
                    this.validateEmail();
                    if (this.isEmailValid()){
                        if (this.allowLoginContinueEmailExist){
                            this.confirmStep();
                        }
                    }
                }
            },

            confirmStep : function(){
                if (this.useStepsConfirmation){
                    this.fromStepConfirmation = true;
                    this.emailHasChanged(true);
                    this.fromStepConfirmation = false;
                    if (this.isEmailValid()){
                        if (this.allowLoginContinueEmailExist){
                            this.stepConfirmed(true);
                        }
                    } else {
                        this.unConfirmStep();
                    }
                }
            },

            /**
             * Check email existing.
             */
            checkEmailAvailability: function () {
                this._super();
                if (this.useStepsConfirmation && !this.allowLoginContinueEmailExist){
                    $.when(this.isEmailCheckComplete).always(function () {
                        this.stepConfirmed(!this.isPasswordVisible());
                    }.bind(this));
                }
            },

            unConfirmStep : function(){
                this.stepConfirmed(false);
            },
        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);
