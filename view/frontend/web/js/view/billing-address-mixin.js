/*jshint browser:true*/
/*global define*/
define([
    'ko',
    'Linets_Checkout/js/model/steps-visibility-manager',
    'Magento_Checkout/js/model/quote',
], function (
    ko,
    stepsVisibilityManager,
    quote
) {
    'use strict';

    var mixin = {
        useStepsConfirmation: stepsVisibilityManager.useStepsConfirmation,
        billingAddressStepConfirmed : stepsVisibilityManager.billingAddressStepConfirmed,

        initObservable: function () {
            this._super();

            if (this.useStepsConfirmation){
                this.isAddressDetailsVisible.subscribe(function (value) {
                    if (value) {
                        this.billingAddressStepConfirmed(true);
                    } else {
                        this.billingAddressStepConfirmed(false);
                    }
                }, this);
            }

            quote.shippingAddress.subscribe(function(){
                this.canUseShippingAddress();
            },this);

            return this;
        },

        // PREVENNT UPDATE ADDRESS
        /**
         * Update address action
         */
        updateAddress: function () {
            if (this.selectedAddress() && !this.isAddressFormVisible()) {
                this._super();
            } else {
                this.source.set('params.invalid', false);
                this.source.trigger(this.dataScopePrefix + '.data.validate');
                if (this.source.get(this.dataScopePrefix + '.custom_attributes')) {
                    this.source.trigger(this.dataScopePrefix + '.custom_attributes.data.validate');
                }
                if (!this.source.get('params.invalid')) {
                    this._super();
                }
            }
        },
    };

    return function (target) {
        return target.extend(mixin);
    };
});
