/*jshint browser:true*/
/*global define*/
define([
    'ko',
    'jquery',
    'Linets_Checkout/js/model/steps-visibility-manager',
    'uiLayout',
    'mage/translate',
    'Magento_InventoryInStorePickupFrontend/js/model/pickup-locations-service'
], function (ko, $, stepsVisibilityManager, layout, $t, pickupLocationsService) {
        'use strict';

        var mixin = {
            useStepsConfirmation: stepsVisibilityManager.useStepsConfirmation,
            loginStepConfirmed : stepsVisibilityManager.loginStepConfirmed,
            showLocationMap: window.checkoutConfig.linets.show_location_map,
            filterByRegion: window.checkoutConfig.linets.pickup_filter_region,
            filterByCity: window.checkoutConfig.linets.pickup_filter_city,
            closestStore: window.checkoutConfig.linets.pickup_closest_store,
            geolocationPosition: ko.observable(false),
            geolocationError: ko.observable(null),
            noFilterNearbyLocations: false,
            othersText: $t('Others'),
            regions : ko.observableArray([]),
            cities : ko.observableArray([]),
            regionSelected: ko.observable(''),
            citySelected: ko.observable(''),
            filteringFlag: false,
            sortingFlag: false,

            initialize: function () {
                // Remove Limit if filters are active
                if (this.filterByRegion || this.filterByCity){
                    this.nearbySearchLimit = null;
                }

                this._super();

                this.initSortStores();
                this.initFilters();

                return this;
            },

            /**
             * @param {Object} location
             * @returns void
             */
            selectPickupLocation: function (location) {
                this.selectedLocation(null);
                return this._super();
            },

            /**
             * @returns void
             */
            openPopup: function () {
                this.updateNearbyLocations('');
                this.getPopup().openModal();
            },

            /**
             * @returns void
             */
            onRender: function(){
                this.popup = null;
                this.getPopup();
                if (!this.nearbyLocations()){
                    this.updateNearbyLocations('');
                }
            },

            /**
             * @returns void
             */
            changeStore: function(){
                this.selectedLocation(null)
            },

            /**
             * @returns void
             */
            initFilters : function(){
                if (this.filterByRegion){
                    if (this.nearbyLocations() && this.nearbyLocations().length){
                        this.initFilterStores();
                    } else {
                        var subscription = this.nearbyLocations.subscribe(function(){
                            subscription.dispose();
                            if (this.nearbyLocations() && this.nearbyLocations().length){
                                this.initFilterStores();
                            }
                        }, this);
                    }
                }
            },

            /**
             * @returns void
             */
            initFilterStores : function(){
                this.noFilterNearbyLocations = this.nearbyLocations();

                this.regionSelected.subscribe(function(){
                    this.setStoresByRegion();
                },this);

                if (this.filterByCity){
                    this.citySelected.subscribe(function(){
                        this.setStoresByCity();
                    },this);
                }

                this.setRegions();
            },

            /**
             * Get list of available regions with stores
             * @returns {Object}
             */
            setRegions: function () {
                let itemsRegions = [],
                    regions = [];

                this.regions([]);

                this.nearbyLocations().forEach(function(store){
                    if (store.region){
                        itemsRegions.push(store.region);
                    } else {
                        itemsRegions.push(this.othersText);
                    }
                }.bind(this));

                itemsRegions.forEach(function(store){
                    if($.inArray(store, regions) === -1) regions.push(store);
                });

                this.regions(regions);
                if (regions.length == 1){
                    this.regionSelected(regions[0]);
                }

                /*if(this.closestStore && this.geolocationPosition()){
                    this.regionSelected(this.nearbyLocations()[0].region)
                }*/
            },

            /**
             * Get list of available cities with stores
             * @returns {Object}
             */
            setCities: function () {
                let itemsCities = [],
                    cities = [];

                this.cities([]);

                this.nearbyLocations().forEach(function(store){
                    if (store.city){
                        itemsCities.push(store.city);
                    } else {
                        itemsCities.push(this.othersText);
                    }
                }.bind(this));

                itemsCities.forEach(function(store){
                    if($.inArray(store, cities) === -1) cities.push(store);
                });

                this.cities(cities);
                if (cities.length === 1){
                    this.citySelected(cities[0]);
                }
            },


            setStoresByRegion: function(){
                this.filteringFlag = true;

                if (this.filterByCity){
                    this.citySelected('');
                    this.cities([]);
                }

                var filteredStores = this.noFilterNearbyLocations;
                if (this.regionSelected()) {
                    filteredStores = []
                    this.noFilterNearbyLocations.forEach(function (store) {
                        if(Object.values(store).includes(this.regionSelected())){
                            filteredStores.push(store);
                        } else {
                            if (this.regionSelected() == this.othersText && !store.region){
                                filteredStores.push(store);
                            }
                        }
                    }.bind(this));
                }

                filteredStores = this.orderStoresByDistance(filteredStores);
                this.nearbyLocations(null);
                this.nearbyLocations(filteredStores);
                if (this.filterByCity){
                    this.setCities();
                }
                this.filteringFlag = false;
            },

            setStoresByCity: function(){
                if (!this.filteringFlag){
                    if (this.regionSelected()) {
                        var regionFilteredStores = [];
                        this.noFilterNearbyLocations.forEach(function (store) {
                            if(Object.values(store).includes(this.regionSelected())){
                                regionFilteredStores.push(store);
                            } else {
                                if (this.regionSelected() == this.othersText && !store.region){
                                    regionFilteredStores.push(store);
                                }
                            }
                        }.bind(this));
                        if (this.citySelected()){
                            var cityFilteredStores = [];
                            regionFilteredStores.forEach(function (store) {
                                if(Object.values(store).includes(this.citySelected())){
                                    cityFilteredStores.push(store);
                                } else {
                                    if (this.citySelected() == this.othersText && !store.city){
                                        cityFilteredStores.push(store);
                                    }
                                }
                            }.bind(this));
                            cityFilteredStores = this.orderStoresByDistance(cityFilteredStores);
                            this.nearbyLocations(null);
                            this.nearbyLocations(cityFilteredStores);
                        } else {
                            regionFilteredStores = this.orderStoresByDistance(regionFilteredStores);
                            this.nearbyLocations(null);
                            this.nearbyLocations(regionFilteredStores);

                        }
                    }
                }
            },

            initSortStores: function(){
                if (this.closestStore){
                    this._loadCurrentPosition();
                    if (this.nearbyLocations() && this.nearbyLocations().length){
                        this.nearbyLocations(this.orderStoresByDistance());
                    }
                }
            },

            orderStoresByDistance: function(stores){
                if (this.sortingFlag){
                    return;
                }
                if (!stores){
                    stores = this.nearbyLocations()
                }
                this.sortingFlag = true;
                if (this.closestStore && stores && stores.length && this.geolocationPosition()){
                    let orderedStores = [];
                    stores.forEach(function(store) {
                        store['closest_store'] = false;
                        store['distance'] = this.getDistance(
                            this.geolocationPosition().coords.latitude,
                            this.geolocationPosition().coords.longitude,
                            store.latitude,
                            store.longitude
                        );
                        orderedStores.push(store);
                    }.bind(this));

                    orderedStores.sort(function(a,b){return parseFloat(a.distance) - parseFloat(b.distance)});
                    orderedStores[0].closest_store = true;

                    stores = orderedStores;
                }
                this.sortingFlag = false;
                return stores;
            },

            _loadCurrentPosition : function() {
                this._getCurrentPosition()
                    .then((position) => {
                        this.geolocationPosition(position);
                        let stores = this.orderStoresByDistance();
                        this.nearbyLocations(null);
                        this.nearbyLocations(stores);
                    })
                    .catch((error) => {
                        console.error('Geolocation error: ' + error.message);
                        this.geolocationError(true)
                    });
            },

            _getCurrentPosition: function () {
                if (navigator.geolocation) {
                    return new Promise(
                        (resolve, reject) => navigator.geolocation.getCurrentPosition(resolve, reject)
                    )
                } else {
                    return new Promise(
                        resolve => resolve({})
                    )
                }
            },

            /**
             * Returns the distance between 2 points by latitude and longitude.
             * @param lat1
             * @param lon1
             * @param lat2
             * @param lon2
             * @returns {number}
             */
            getDistance: function (lat1, lon1, lat2, lon2) {
                let radlat1 = Math.PI * lat1/180,
                    radlat2 = Math.PI * lat2/180,
                    theta = lon1-lon2,
                    radtheta = Math.PI * theta/180,
                    dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

                if (dist > 1) {
                    dist = 1;
                }

                dist = Math.acos(dist);
                dist = dist * 180/Math.PI;
                dist = dist * 60 * 1.1515;
                dist = dist * 1.609344; //KM

                return dist
            }
        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);
