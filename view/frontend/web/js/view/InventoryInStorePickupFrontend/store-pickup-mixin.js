/*jshint browser:true*/
/*global define*/
define([
    'jquery',
    'uiRegistry',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/model/step-navigator'
], function (
    $,
    registry,
    quote,
    checkoutDataResolver,
    stepNavigator
) {
        'use strict';

        var mixin = {
            deliveryHtmlLabel: window.checkoutConfig.linets.delivery_html,
            storePickupHtmlLabel: window.checkoutConfig.linets.store_pickup_html,
            lastShippingAddress: null,

            initialize: function(){
                this._super();
            },

            /**
             * @returns void
             */
            selectShipping: function () {
                var nonPickupShippingMethod = _.find(
                    this.rates(),
                    function (rate) {
                        return (
                            rate['carrier_code'] !== this.rate['carrier_code'] &&
                            rate['method_code'] !== this.rate['method_code']
                        );
                    },
                    this
                );

                this.selectShippingMethod(nonPickupShippingMethod);

                if (quote.shippingAddress().getType() == 'store-pickup-address'){
                    if (this.lastShippingAddress && this.lastShippingAddress.getType() != 'store-pickup-address'){
                        quote.shippingAddress(this.lastShippingAddress);
                        checkoutDataResolver.resolveShippingAddress();
                    } else {
                        checkoutDataResolver.resolveShippingAddress();
                        registry.async('checkoutProvider')(function (checkoutProvider) {
                          checkoutProvider.set(
                              'shippingAddress',
                              quote.shippingAddress()
                          );
                          checkoutProvider.trigger('data.reset');
                      });
                    }

                    if (quote.shippingAddress().getType() == 'store-pickup-address'){
                        quote.shippingAddress(
                            $.extend({}, quote.shippingAddress(), {
                                /**
                                 * Is address can be used for billing
                                 *
                                 * @return {Boolean}
                                 */
                                canUseForBilling: function () {
                                    return true;
                                },

                                /**
                                 * Returns address type
                                 *
                                 * @return {String}
                                 */
                                getType: function () {
                                    return 'new-customer-address';
                                }
                            })
                        );
                    }
                }
            },

            /**
             * @returns void
             */
            selectStorePickup: function () {
                this.lastShippingAddress = quote.shippingAddress();
                this._super();
            },

            /**
             * Synchronize store pickup visibility with shipping step.
             *
             * @returns void
             */
            syncWithShipping: function () {
                var shippingStep = _.findWhere(stepNavigator.steps(), {
                    code: 'shipping'
                });
                if (shippingStep){
                    this._super();
                }
            },
        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);
