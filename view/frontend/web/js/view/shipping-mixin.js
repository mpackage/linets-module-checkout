/*jshint browser:true*/
/*global define*/
define([
    'jquery',
    'ko',
    'Linets_Checkout/js/model/steps-visibility-manager',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/select-shipping-address',
    'uiRegistry',
    'mage/translate',
    'Magento_Customer/js/customer-data'
], function (
    $,
    ko,
    stepsVisibilityManager,
    customer,
    addressConverter,
    quote,
    selectShippingAddress,
    registry,
    $t,
    customerData
) {
    'use strict';

    var countryData = customerData.get('directory-data');

    var mixin = {
        useStepsConfirmation: stepsVisibilityManager.useStepsConfirmation,
        loginStepConfirmed : stepsVisibilityManager.loginStepConfirmed,
        shippingAddressStepConfirmed : stepsVisibilityManager.shippingAddressStepConfirmed,
        shippingMethodStepConfirmed : stepsVisibilityManager.shippingMethodStepConfirmed,
        preventSelectInStorePickup : stepsVisibilityManager.preventSelectInStorePickup,
        address: quote.shippingAddress,
        shippingAddressConfirmedTemplate: 'Magento_Checkout/shipping-address/address-renderer/inline',
        storePickupRate: {
            'carrier_code': 'instore',
            'method_code': 'pickup'
        },
        deliveryRates : [],

        initialize: function () {
            this._super();

            this.rates.subscribe(function(){
                this.deliveryRates = _.filter(this.rates(), function (rate) {
                    return (
                        rate['carrier_code'] !== this.storePickupRate['carrier_code'] &&
                        rate['method_code'] !== this.storePickupRate['method_code']
                    );
                }, this);
            }, this);

            if (this.useStepsConfirmation){
                if (!this.isFormInline){
                    this.shippingAddressStepConfirmed(true);
                }
            }

            return this;
        },

        afterFormRendered: function(){
            if (this.isFormInline && this.useStepsConfirmation && !this.shippingAddressStepConfirmed()){
                let $additionalFieldset = this.getRegion('additional-fieldsets')()[0];
                if ($additionalFieldset){
                    if (this.address()){
                        this.confirmShippingAddress();
                    }
                    this.resetErrorComponents($additionalFieldset.elems());
                }
            }
        },

        resetErrorComponents : function(elems){
            elems.forEach(function (component) {
                if (typeof(component.reset) == 'function'){
                    if (typeof(component.error) == 'function'){
                        if (component.error()){
                            component.reset();
                        }
                    }
                }
                if (typeof(component.elems) == 'function') {
                    if (component.elems().length) {
                        this.resetErrorComponents(component.elems())
                    }
                }
            }, this);
        },

        confirmShippingAddress: function(){
            if (this.isFormInline){
                if (this.validateShippingAddressInformation()){
                    this.shippingAddressStepConfirmed(true);
                } else{
                    if (this.useStepsConfirmation){
                        this.shippingAddressStepConfirmed(false);
                    }
                }
            }
        },

        unConfirmShippingAddress: function(){
            if (this.useStepsConfirmation){
                this.shippingAddressStepConfirmed(false);
            }
        },

        /**
         * @return {Boolean}
         */
        validateShippingAddressInformation: function () {
            var shippingAddress,
                addressData,
                field,
                option = _.isObject(this.countryOptions) && this.countryOptions[quote.shippingAddress().countryId],
                messageContainer = registry.get('checkout.errors').messageContainer;

            if (this.isFormInline) {
                this.source.set('params.invalid', false);
                this.triggerShippingDataValidateEvent();

                if (this.source.get('params.invalid')) {
                    this.focusInvalid();

                    return false;
                }

                shippingAddress = quote.shippingAddress();
                addressData = addressConverter.formAddressDataToQuoteAddress(
                    this.source.get('shippingAddress')
                );

                //Copy form data to quote shipping address object
                for (field in addressData) {
                    if (addressData.hasOwnProperty(field) &&  //eslint-disable-line max-depth
                        shippingAddress.hasOwnProperty(field) &&
                        typeof addressData[field] != 'function' &&
                        _.isEqual(shippingAddress[field], addressData[field])
                    ) {
                        shippingAddress[field] = addressData[field];
                    } else if (typeof addressData[field] != 'function' &&
                        !_.isEqual(shippingAddress[field], addressData[field])) {
                        shippingAddress = addressData;
                        break;
                    }
                }

                if (customer.isLoggedIn()) {
                    shippingAddress['save_in_address_book'] = 1;
                }

                selectShippingAddress(shippingAddress);
            } else if (customer.isLoggedIn() &&
                option &&
                option['is_region_required'] &&
                !quote.shippingAddress().region
            ) {
                messageContainer.addErrorMessage({
                    message: $t('Please specify a regionId in shipping address.')
                });

                return false;
            }

            return true;
        },

        /**
         * @param {String} countryId
         * @return {String}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },

        /**
         * Get customer attribute label
         *
         * @param {*} attribute
         * @returns {*}
         */
        getCustomAttributeLabel: function (attribute) {
            var label;

            if (typeof attribute === 'string') {
                return attribute;
            }

            if (attribute.label) {
                return attribute.label;
            }

            if (_.isArray(attribute.value)) {
                label = _.map(attribute.value, function (value) {
                    return this.getCustomAttributeOptionLabel(attribute['attribute_code'], value) || value;
                }, this).join(', ');
            } else {
                label = this.getCustomAttributeOptionLabel(attribute['attribute_code'], attribute.value);
            }

            return label || attribute.value;
        },


        /**
         * Get option label for given attribute code and option ID
         *
         * @param {String} attributeCode
         * @param {String} value
         * @returns {String|null}
         */
        getCustomAttributeOptionLabel: function (attributeCode, value) {
            var option,
                label,
                options = this.source.get('customAttributes') || {};

            if (options[attributeCode]) {
                option = _.findWhere(options[attributeCode], {
                    value: value
                });

                if (option) {
                    label = option.label;
                }
            }

            return label;
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
