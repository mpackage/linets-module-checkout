define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
],function ($, wrapper, quote){
    'use strict';

    var storePickupRate = {
        'carrier_code': 'instore',
        'method_code': 'pickup'
    };

    return function (target) {
        /**
         * @param {Object} ratesData
         */
        target.resolveShippingRates = wrapper.wrap(
            target.resolveShippingRates, function(originalResolveShippingRates, ratesData){

                if (!quote.shippingMethod() ||
                    quote.shippingMethod().carrier_code !== storePickupRate.carrier_code ||
                    quote.shippingMethod().method_code !== storePickupRate.method_code
                ){
                    ratesData = _.filter(ratesData, function (rate) {
                        return (
                            rate['carrier_code'] !== storePickupRate['carrier_code'] &&
                            rate['method_code'] !== storePickupRate['method_code']
                        );
                    }, this);
                }

                return originalResolveShippingRates(ratesData);
            });

        return target;
    };
});
