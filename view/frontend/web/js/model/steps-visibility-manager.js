define([
    'ko',
    'jquery',
    'Magento_Customer/js/model/customer',
],
function(
    ko,
    $,
    customer
) {
    'use strict';

    var useStepConfirmation = window.checkoutConfig.linets.use_steps_confirmation,
    loginStepConfirmed = (customer.isLoggedIn())? true : !useStepConfirmation;

    if (useStepConfirmation){
        $('#checkout').addClass('steps-confirmation-active');
    }
    return {
        useStepsConfirmation: useStepConfirmation,
        loginStepConfirmed: ko.observable(loginStepConfirmed),
        allowLoginContinueEmailExist: window.checkoutConfig.linets.allow_continue_email_exist,
        shippingAddressStepConfirmed: ko.observable(!useStepConfirmation),
        shippingMethodStepConfirmed: ko.observable(!useStepConfirmation),
        billingAddressStepConfirmed: ko.observable(!useStepConfirmation),
        preventSelectInStorePickup: ko.observable(false),
    };
});
