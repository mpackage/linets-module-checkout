define([
    'jquery',
    'mage/utils/wrapper'
],function ($, wrapper){
    'use strict';

    return function (target) {
        target.formAddressDataToQuoteAddress = wrapper.wrap(
            target.formAddressDataToQuoteAddress, function(originalQuoteAddressData, formData){
                if (formData && formData['custom_attributes'] && $.isArray(formData['custom_attributes'])){
                    let customAttributes = formData['custom_attributes'];
                    formData['custom_attributes'] = {};
                    _.map(customAttributes,
                        function (customAttribute, key) {
                            let attributeCode = customAttribute.attribute_code,
                                value = customAttribute.value;
                            if (customAttribute.value && $.isPlainObject(customAttribute.value)){
                                if (customAttribute.value.attribute_code && customAttribute.value.value){
                                    attributeCode = customAttribute.value.attribute_code;
                                    value = customAttribute.value.value;
                                }
                            }
                            formData['custom_attributes'][attributeCode] = value;
                        }
                    );
                }

                return originalQuoteAddressData(formData);
            });

        return target;
    };
});
