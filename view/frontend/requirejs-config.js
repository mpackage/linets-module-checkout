var config = {
    config: {
        mixins: {
            "Magento_Checkout/js/model/address-converter":{
                "Linets_Checkout/js/model/address-converter-mixin": true
            },
            "Magento_Checkout/js/model/checkout-data-resolver":{
                "Linets_Checkout/js/model/checkout-data-resolver-mixin": true
            },
            "Magento_Checkout/js/view/form/element/email":{
                "Linets_Checkout/js/view/form/element/email-mixin": true
            },
            "Magento_Checkout/js/view/shipping":{
                "Linets_Checkout/js/view/shipping-mixin": true
            },
            "Magento_Checkout/js/view/billing-address":{
                "Linets_Checkout/js/view/billing-address-mixin": true
            },
            "Magento_Checkout/js/view/payment":{
                "Linets_Checkout/js/view/payment-mixin": true
            },
            "Magento_InventoryInStorePickupFrontend/js/view/store-pickup":{
                "Linets_Checkout/js/view/InventoryInStorePickupFrontend/store-pickup-mixin": true
            },
            "Magento_InventoryInStorePickupFrontend/js/view/store-selector":{
                "Linets_Checkout/js/view/InventoryInStorePickupFrontend/store-selector-mixin": true
            }
        }
    }
};
